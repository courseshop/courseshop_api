"use strict"
const jose  =  require("jose");
const createError =  require("http-errors");
const redis =  require("../config/init_redis");

async function GenerateAccessToken (payload) {
    const token = await jose.JWT.sign({
        'role': payload.role,
        'vigence': payload.vigence,
        'email': payload.email,
        "displayName": payload.displayName
    }, process.env.KEY_TOKEN_SECRET, {
        audience: ""+payload._id,
        issuer: 'localhost:5000',
        expiresIn: '8 h',
        now: new Date(),
        header: {
          typ: 'JWT',
        }
      })
    return token
}
function VerifyAccessToken (req, res, next) {
    if(!req.headers["authorization"]) return next(createError.Unauthorized())
    try {
        const token = req.headers["authorization"].split(" ")[1];
        const payload = jose.JWT.verify(token, process.env.KEY_TOKEN_SECRET)
        req.payload = payload;
       return  next();
    } catch (error) {
        return next(createError.Unauthorized("Unauthorized"))
    }
}

async function GenerateRefreshToken (payload) {
    const refreshToken = await jose.JWT.sign({}, process.env.KEY_REFRESH_TOKEN_SECRET, {
        audience: String(payload._id),
        issuer: 'localhost:5000',
        expiresIn: '604800 s',
        now: new Date(),
        header: {
          typ: 'JWT',
        }
      })
      const saveToRedis = await redis.SET(String(payload._id), refreshToken, 'EX', 60*60*24*7)
    return refreshToken
}
 function VerifyRefreshToken (refreshToken) {
  return new Promise((resolve, reject) => {
    try {
        const payload =  jose.JWT.verify(refreshToken, process.env.KEY_REFRESH_TOKEN_SECRET)
        const clientId = payload.aud 
        redis.GET(clientId, (err, value) => {
          if(err) return reject(createError.InternalServerError())
          if(refreshToken === value) return resolve(clientId)
          return reject(createError.Unauthorized())
        })
    } catch (error) {
        return reject(createError.Unauthorized("Unauthorized"))
    }
  })
}

module.exports = {
    GenerateAccessToken,
    VerifyAccessToken,
    GenerateRefreshToken,
    VerifyRefreshToken,
}