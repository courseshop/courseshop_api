"use strict"
const  Joi = require('@hapi/joi');
const ProfileSaveSchema = Joi.object({
    nombres: Joi.string().lowercase().required(),
    apellidos: Joi.string().lowercase().required(),
    dni_ce: Joi.string().lowercase().required(),
    fecha_nacimiento: Joi.date().required(),
    telefono: Joi.string().lowercase().allow("").optional(""),
    imagen: Joi.string().allow("").optional(""),
    estado_civil: Joi.string().lowercase().allow("").optional(""),
    grado_instruccion: Joi.string().allow("").optional(""),
    vigence: Joi.string().allow("").optional(""),
    state: Joi.string().allow("").optional(""),
})

const ProfileUpdateSchema = Joi.object({
    nombres: Joi.string().allow("").optional(""),
    apellidos: Joi.string().allow("").optional(""),
    dni_ce: Joi.string().allow("").optional(""),
    fecha_nacimiento: Joi.date().allow("").optional(""),
    telefono: Joi.string().lowercase().allow("").optional(""),
    imagen: Joi.string().allow("").optional(""),
    estado_civil: Joi.string().lowercase().allow("").optional(""),
    grado_instruccion: Joi.string().allow("").optional(""),
    vigence: Joi.string().allow("").optional(""),
    state: Joi.string().allow("").optional(""),
})
const IdProfileSaveSchema = Joi.object({
    id: Joi.string().required(),
    permanent: Joi.boolean().allow("").optional(""),
    all: Joi.boolean().allow("").optional("")
})
module.exports = {
    ProfileSaveSchema,
    ProfileUpdateSchema,
    IdProfileSaveSchema
}
