"use strict"
const  Joi = require('@hapi/joi');
const RolesSaveSchema = Joi.object({
    name: Joi.string().lowercase().required(),
    vigence: Joi.boolean().default("true").allow("").optional(""),
    state: Joi.boolean().default("true").allow("").optional(""),
    privilege: Joi.string().lowercase().allow("").optional(""),
})

const IdSchema = Joi.object({
    id: Joi.string().required(),
    permanent: Joi.boolean().allow("").optional(""),
    all: Joi.boolean().allow("").optional("")
})
module.exports = {
    RolesSaveSchema,
    IdSchema
}
