"use strict"
const  Joi = require('@hapi/joi');
const SignUpSchema = Joi.object({
    nombres: Joi.string().lowercase().required().messages({
        'string.base': `El campo "nombres" es de tipo texto`,
        'string.empty': `El campo "nombres" esta vacio!`,
        'any.required': `El campo "nombres" es requerido!`
    }),
    apellidos: Joi.string().lowercase().required().messages({
        'string.base': `El campo "apellidos" es de tipo texto`,
        'string.empty': `El campo "apellidos" esta vacio!`,
        'any.required': `El campo "apellidos" es requerido!`
            }),
    dni_ce: Joi.string().min(8).max(10).lowercase().required().messages({
        'string.base': `El campo "DNI/CE" es de tipo texto`,
        'string.empty': `El campo "DNI/CE" esta vacio!`,
        'any.required': `El campo "DNI/CE" es requerido!`,
        'string.min': `El campo "DNI/CE" tiene un tamaño minimo de {#limit}`,
        'string.max': `El campo "DNI/CE" tiene un tamaño maximo de {#limit}`,
            }),
    fecha_nacimiento: Joi.string().required().messages({
        'string.base': `El campo "fecha de nacimiento" es de tipo texto`,
        'string.empty': `El campo "fecha de nacimiento" esta vacio!`,
        'any.required': `El campo "fecha de nacimiento" es requerido!`
            }),
    telefono: Joi.string().required().messages({
        'string.base': `El campo "telefono" es de tipo texto`,
        'string.empty': `El campo "telefono" esta vacio!`,
        'any.required': `El campo "telefono" es requerido!`
            }),
    email: Joi.string().email().required().messages({
        'string.base': `El campo "email" es de tipo texto`,
        'string.empty': `El campo "email" esta vacio!`,
        'any.required': `El campo "email" es requerido!`
            }),
    password: Joi.string().min(6).required().messages({
        'string.base': `El campo "password" es de tipo texto`,
        'string.empty': `El campo "password" esta vacio!`,
        'any.required': `El campo "password" es requerido!`
            }),

})
const SignInSchema = Joi.object({
    email: Joi.string().email().required().messages({
        'string.base': `El campo "email" es de tipo texto`,
        'string.empty': `El campo "email" esta vacio!`,
        'any.required': `El campo "email" es requerido!`
            }),
    password: Joi.string().min(6).required().messages({
        'string.base': `El campo "password" es de tipo texto`,
        'string.empty': `El campo "password" esta vacio!`,
        'any.required': `El campo "password" es requerido!`
            }),
    displayName: Joi.string().lowercase().allow("").optional("").messages({
        'string.base': `El campo "nombres" es de tipo texto`,
        'string.empty': `El campo "nombres" esta vacio!`,
        'any.required': `El campo "nombres" es requerido!`
            })
})
const ResetCodeSchema = Joi.object({
    email: Joi.string().email().required().messages({
        'string.base': `El campo "email" es de tipo texto`,
        'string.empty': `El campo "email" esta vacio!`,
        'any.required': `El campo "email" es requerido!`
            })
})
const ResetPasswordSchema = Joi.object({
    email: Joi.string().email().required().messages({
        'string.base': `El campo "email" es de tipo texto`,
        'string.empty': `El campo "email" esta vacio!`,
        'any.required': `El campo "email" es requerido!`
            }),
    password: Joi.string().min(6).required().messages({
        'string.base': `El campo "password" es de tipo texto`,
        'string.empty': `El campo "password" esta vacio!`,
        'any.required': `El campo "password" es requerido!`
            }),
    code: Joi.string().lowercase().required().messages({
        'string.base': `El campo "codigo de reseteo" es de tipo texto`,
        'string.empty': `El campo "codigo de reseteo" esta vacio!`,
        'any.required': `El campo "codigo de reseteo" es requerido!`
            })
})
const EditPasswordSchema = Joi.object({
    password: Joi.string().min(6).required().messages({
        'string.base': `El campo "password" es de tipo texto`,
        'string.empty': `El campo "password" esta vacio!`,
        'any.required': `El campo "password" es requerido!`
            }),
    newPassword: Joi.string().min(6).required().messages({
        'string.base': `El campo "nuevo password" es de tipo texto`,
        'string.empty': `El campo "nuevo password" esta vacio!`,
        'any.required': `El campo "nuevo password" es requerido!`
            })
})
const EditEmailSchema = Joi.object({
    password: Joi.string().min(6).required().messages({
        'string.base': `El campo "nuevo password" es de tipo texto`,
        'string.empty': `El campo "nuevo password" esta vacio!`,
        'any.required': `El campo "nuevo password" es requerido!`
            }),
    email: Joi.string().email().required().messages({
        'string.base': `El campo "email" es de tipo texto`,
        'string.empty': `El campo "email" esta vacio!`,
        'any.required': `El campo "email" es requerido!`
            })
})
const EditProfileSchema = Joi.object({
    nombres: Joi.string().trim().lowercase().allow("").optional("").messages({
        'string.base': `El campo "nombres" es de tipo texto`,
        'string.empty': `El campo "nombres" esta vacio!`,
        'any.required': `El campo "nombres" es requerido!`
            }),
    apellidos: Joi.string().trim().lowercase().allow("").optional("").messages({
        'string.base': `El campo "apellidos" es de tipo texto`,
        'string.empty': `El campo "apellidos" esta vacio!`,
        'any.required': `El campo "apellidos" es requerido!`
            }),
    // dni_ce: Joi.string().trim().lowercase().allow("").optional(""),
    fecha_nacimiento: Joi.string().trim().lowercase().allow("").optional("").messages({
        'string.base': `El campo "fecha de nacimiento" es de tipo texto`,
        'string.empty': `El campo "fecha de nacimiento" esta vacio!`,
        'any.required': `El campo "fecha de nacimiento" es requerido!`
            }),
    telefono: Joi.string().lowercase().trim().allow("").optional("").messages({
        'string.base': `El campo "telefono" es de tipo texto`,
        'string.empty': `El campo "telefono" esta vacio!`,
        'any.required': `El campo "telefono" es requerido!`
            }),
    imagen: Joi.string().lowercase().trim().allow("").optional("").messages({
        'string.base': `El campo "imagen" es de tipo texto`,
        'string.empty': `El campo "imagen" esta vacio!`,
        'any.required': `El campo "imagen" es requerido!`
            }),
    estado_civil: Joi.string().lowercase().trim().allow("").optional("").messages({
        'string.base': `El campo "estado civil" es de tipo texto`,
        'string.empty': `El campo "estado civil" esta vacio!`,
        'any.required': `El campo "estado civil" es requerido!`
            }),
    grado_instruccion: Joi.string().lowercase().trim().allow("").optional("").messages({
        'string.base': `El campo "grado de instruccion" es de tipo texto`,
        'string.empty': `El campo "grado de instruccion" esta vacio!`,
        'any.required': `El campo "grado de instruccion" es requerido!`
            }),
})
module.exports = {
    SignUpSchema,
    SignInSchema,
    ResetCodeSchema,
    ResetPasswordSchema,
    EditPasswordSchema,
    EditEmailSchema,
    EditProfileSchema
}
