"use strict"
const  Joi = require('@hapi/joi');
const SearchScheme = Joi.object({
    id: Joi.string().default(0).allow("").optional(""),
    search: Joi.string().default(0).allow("").optional(""),
    page: Joi.number().default(0).allow("").optional(""),
    limit: Joi.number().default(30).allow("").optional(""),
    search: Joi.string().default("").allow("").optional(""),
    orderby: Joi.string().default("updatedAt").allow("").optional(""),
    groupby: Joi.string().default("updatedAt").allow("").optional(""),
})

module.exports = {
    SearchScheme
}
