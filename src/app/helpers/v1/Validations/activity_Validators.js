"use strict"
const  Joi = require('@hapi/joi');
const IdActivitySchema = Joi.object({
    id: Joi.string().required(),
    permanent: Joi.boolean().allow("").optional(""),
    all: Joi.boolean().allow("").optional(""),
    start:Joi.string().required(),
    end:Joi.string().required(),
})
module.exports = {
    IdActivitySchema
}
