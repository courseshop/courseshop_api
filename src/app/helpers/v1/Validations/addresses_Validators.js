"use strict"
const  Joi = require('@hapi/joi');
const AddressesSaveSchema = Joi.object({
    contry: Joi.string().lowercase().required(),
    departament: Joi.string().lowercase().required(),
    province: Joi.string().lowercase().required(),
    addresses: Joi.string().lowercase().required(),
    phones: Joi.string().lowercase().required(),
    vigence: Joi.boolean().default("true").allow("").optional(""),
    profile: Joi.string().allow("").optional(""),
})
const AddressesUpdateSchema = Joi.object({
    contry: Joi.string().lowercase().allow("").optional(""),
    departament: Joi.string().lowercase().allow("").optional(""),
    province: Joi.string().lowercase().allow("").optional(""),
    addresses: Joi.string().lowercase().allow("").optional(""),
    phones: Joi.string().lowercase().allow("").optional(""),
    vigence: Joi.boolean().default("true").allow("").optional(""),
    profile: Joi.string().allow("").optional(""),
})
const IdAddressesSchema = Joi.object({
    id: Joi.string().required(),
    permanent: Joi.boolean().allow("").optional(""),
    all: Joi.boolean().allow("").optional("")
})
module.exports = {
    AddressesSaveSchema,
    AddressesUpdateSchema,
    IdAddressesSchema
}
