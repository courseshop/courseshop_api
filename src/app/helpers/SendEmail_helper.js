"use strict"
const nodemailer =  require("nodemailer");
const dotenv = require("dotenv");
const createError= require("http-errors")
dotenv.config({path: 'config.env'})
async function sendMail(to, subject, text, html) {
  try {    
    const transporter = nodemailer.createTransport({
      host: process.env.NODEMAILER_HOST,
      port: process.env.NODEMAILER_PORT,
      secure: false,
      auth: {
        user: process.env.NODEMAILER_USER,
        pass: process.env.NODEMAILER_PASS
      },
      tls: {
        rejectUnauthorized: false
      }
      
  });
  const mail = await transporter.sendMail({
      from: `${process.env.NAME_APP} <${process.env.NODEMAILER_FROM}>`, // sender address
      to: to, //"calberto185@gmail.com", // list of receivers
      subject: subject, //"Hello ✔", // Subject line
      text: text, //"Hello world?", // plain text body
      html: html, //"<b>Hello world?</b>", // html body
  })
  await transporter.verify(function(error, success) {
      if (error) {
        throw createError.InternalServerError()
        return ;
      } else {
        // console.log("Server is ready to take our messages");
        // console.log(success)
        // return resolve({message: "Server is ready to take our messages"})
      }
    });
  } catch (error) {
    throw createError.InternalServerError()
    return ;
  }
}
module.exports = sendMail