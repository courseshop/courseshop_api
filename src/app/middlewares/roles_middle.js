const createError = require("http-errors")
module.exports = {
    IsRoleAdmin: (req, res, next) => {
        const user = req.payload
        if(user.role === "admin") return next()
        next(createError.Unauthorized("Insufficient privileges to access this resource"))
    },
    IsRoleSuperAdmin: (req, res, next) => {
        const user = req.payload
        if(user.role === "super") return next()
        next(createError.Unauthorized("Insufficient privileges to access this resource"))
    },
}