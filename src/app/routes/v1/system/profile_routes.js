"use strict"
const routeAccount =  require("express").Router();
const { 
    EditEmail,
    EditPassword,
    Identity,
    EditProfile,
    EditAddresses,
    GetAddresses,
    ReadUser,
    SaveUser,
    UpdateUser,
    DeleteUser,
    DeleteAccount,
    UpdateAccount
 } = require("../../../controllers/v1/system/profile_CTRL");
 const { IsRoleSuperAdmin } = require("../../../middlewares/roles_middle");
 const { VerifyAccessToken } = require("../../../helpers/JWT_helper")

/**
 * @openapi
 *  /me:
 *      get:
 *          description: Muestra el perfil de la cuenta inicializada
 *          tags:
 *              - Mi Perfil
 *          responses:
 *              200:
 *                  description: El perfil se ha obtenido exitosamente
 *              '500':
 *                  description: Hubo un error interno, Intentar mas tarde
 */
routeAccount.get("/me", VerifyAccessToken, Identity)
routeAccount.put("/me", VerifyAccessToken, EditProfile)
routeAccount.put("/me/changeEmail", VerifyAccessToken, EditEmail)
routeAccount.put("/me/changePassword", VerifyAccessToken, EditPassword)
routeAccount.put("/me/addresses", VerifyAccessToken, EditAddresses)
routeAccount.get("/me/addresses", VerifyAccessToken, GetAddresses)
routeAccount.get("/users",  ReadUser)
routeAccount.post("/users",  SaveUser)
routeAccount.put("/users/:userId",  UpdateUser)
routeAccount.delete("/users/:userId",  DeleteAccount)
routeAccount.put("/users/:userId/account/:accoundId",  UpdateAccount)
routeAccount.delete("/users/:userId/account/:accoundId",  DeleteUser)

module.exports = routeAccount;
