"use strict"
const routeAccount =  require("express").Router();
const { 
    refreshToken,
    resetCode,
    resetPassword,
    signin,
    signout,
    signup
 } = require("../../../controllers/v1/system/account_CTRL");
 const { VerifyAccessToken } = require("../../../helpers/JWT_helper")
routeAccount.post("/refreshToken", VerifyAccessToken,  refreshToken)
routeAccount.post("/resetCode", resetCode)
routeAccount.post("/resetPassword", resetPassword)

/**
 * @openapi
 *  /auth/signin:
 *    post:
 *      description: los usuarios inician sesion, mediante su credencial (email y password)
 *      tags:
 *         - Autenticacion
 *      responses:
 *          200:
 *              description: El usuario se ha autenticado satisfactoriamente
 *          404:
 *              description: El usuario no se ha encontrado
 *          500:
 *              description: Ha habido un error interno, intentar mas tarde
 */
routeAccount.post("/signin", signin)



/**
 * @openapi
 *  /auth/signout:
 *    post:
 *      description: los usuarios finalizan su sesion de acceso en el sistema 
 *      tags:
 *         - Autenticacion
 *      responses:
 *          200:
 *              description: El usuario se ha finalizado su sesion satisfactoriamente
 *          500:
 *              description: Ha habido un error interno, intentar mas tarde
 */
routeAccount.post("/signout", VerifyAccessToken, signout)
// routeAccount.post("/signup", signup)

module.exports = routeAccount;
