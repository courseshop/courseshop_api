"use strict"
const routes =  require("express").Router();
const roles= require("../../../controllers/v1/system/roles_CTRL");
 const { IsRoleSuperAdmin } = require("../../../middlewares/roles_middle");
 const { VerifyAccessToken } = require("../../../helpers/JWT_helper")
routes.get("/roles",  roles.get)
routes.post("/roles",  roles.save)
routes.put("/roles",  roles.update)
routes.delete("/roles",  roles.remove)

module.exports = routes;
