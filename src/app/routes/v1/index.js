"use strict"
const routesV1 =  require("express").Router();
const AccountRoutes = require("./system/account_routes");
const ProfileRoutes = require("./system/profile_routes");
routesV1.use("/auth", AccountRoutes);
routesV1.use("", ProfileRoutes);
module.exports = routesV1;
