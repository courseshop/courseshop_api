"use strict"
const router =  require("express").Router();
const addresses= require("../../../controllers/v1/profiles/addresses_CTRL");
 const { IsRoleSuperAdmin } = require("../../../middlewares/roles_middle");
 const { VerifyAccessToken } = require("../../../helpers/JWT_helper")
router.get("/addresses",  addresses.get)
router.post("/addresses",  addresses.save)
router.delete("/addresses",  addresses.remove)

module.exports = router;
