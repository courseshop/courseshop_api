"use strict"
const express =  require("express");
const https =  require("https");
const moment = require("moment");
const compression = require("compression");
const socket = require("socket.io")
const limitter = require("express-rate-limit");
const fs =  require("fs");
const cors =  require("cors");
const morgan =  require("morgan");
const dotenv = require("dotenv");
const path =  require("path");
const swJsDoc = require("swagger-jsdoc")
const swaggerUi = require("swagger-ui-express")
const app = express();
require("./config/init_app")
require("./config/init_db")
require("./config/init_redis")
dotenv.config({path: 'config.env'})
app.use(express.urlencoded({extended: true, limit: '50mb'}))
app.use(express.json({limit: '50mb' }))

app.use(cors({
    "origin": "*",
    // "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    // "preflightContinue": false,
    "optionsSuccessStatus": 204
}))


// app.use(morgan("common"))
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swJsDoc(require("../../doc_swagger.json"), { 
    explorer: true,
    customCss: `
    .topbar {
        background-color: #1b1b1b;
        padding: 10px 0;
        display: none;
    }
    `
 })))
app.post("/todo", (req, res, next) => {
    console.log("body", req.body);
    console.log("params", req.params);
    console.log("headers", req.headers);
    console.log("query", req.query);
    return {
        body: req.body,
        params: req.params,
        headers: req.headers,
        query: req.query,
    }
})
app.use(compression({
    level:6,
    threshold: 100 * 1000,
    filter:(req, res) => {
        if(req.headers["x-no-compresion"]) {
            return false
        }
        return compression.filter(req, res)
    }
}))
app.use(limitter({
    windowMs: 10000,
    max: 100,
    message: {
        error: {
            code: 429,
            message: 'Too many request, please try again later'
        }
    }
}))
app.use("/api/v1", require("./routes/v1/index"))

app.use("/api", async(req, res, next) => {
    next(require('http-errors').NotFound("This resource does not exist!.") )
})
app.use("/api", (err, req, res, next) => {
    res.status(err.status || 500);
    res.send({
        error: {
            status: err.status || 500,
            message: err.message
        }
    })
})
const verifu = require("./helpers/JWT_helper");
const swaggerJSDoc = require("swagger-jsdoc");

app.use("/storage", express.static(path.join(__dirname, "../../storage/")));
// app.use("/expedients", express.static(path.join(__dirname, "../../storage/documents/outputs/")));

app.use(express.static("src/views"));
if(process.env.ssl) {
    const io = socket(https.createServer({
        key: fs.readFileSync(path.join(__dirname, "../cert/key.pem")),
        cert: fs.readFileSync(path.join(__dirname, "../cert/cert.pem"))
    }, app).listen(process.env.PORT ,  () => {
        console.log(`app is running to port https://${process.env.HOST}:${process.env.PORT}`)
    }))

    io.on("connect", (cli) => {
        console.log("cli.id: ", cli.id)
        // global.IO = cli
    })
} else {
    const io = socket(app.listen(process.env.PORT ,  () => {
        console.log(`app is running to port http://${process.env.HOST}:${process.env.PORT}`)
    }),{
        
    })
}
