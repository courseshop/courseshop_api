const dotenv = require("dotenv").config("../../config.env")
if(!process.env.NODE_ENV) {
    process.env.NODE_ENV = "development";
    process.env.PORT = process.env.PORT || "9000";
    process.env.HOST = "127.0.0.1";
} else if(process.env.NODE_ENV === "production"){
    process.env.NODE_ENV = "production";
    process.env.PORT = process.env.PORT;
    process.env.HOST = process.env.HOST;
}
process.on("SIGINT", () => {
    
})