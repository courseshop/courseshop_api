"use strict"
const mongoose =  require("mongoose");
const dotenv = require("dotenv");
dotenv.config({path: 'config.env'})
mongoose.connect(process.env.MONGODB_URI || process.env.MONGODB_LAB, { 
    useNewUrlParser: true, 
    useUnifiedTopology: true, 
    useFindAndModify: false,
    useCreateIndex: true,
    
}).then( (db) => {
    console.log("MongoDB connected")

}).catch(err => {
    console.log(err.message)
})
mongoose.connection.on("connected", (db) => {
    console.log("Mongoose connected to MongoDB")
})
mongoose.connection.on("disconnected", () => {
    console.log("Mongoose is disconnected to db")
})
mongoose.connection.on("error", (err) => {
    console.log(err.mongoose)
})
process.on("SIGINT", async () => {
   await  mongoose.connection.close();
    process.exit(0);
})