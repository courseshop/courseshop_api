"use strict"
const redis = require("redis")
const client =  redis.createClient({
    host: process.env.REDIS_URL,
    port: process.env.REDIS_PORT
})
client.on("connect", () => {
    console.log("Client redis is connected")
})
client.on("error", (err) => {
    console.log(err.message)
})
client.on("ready", () => {
    console.log("Client redis is ready to use...")
})
client.on("end", () => {
    console.log("Client is disconnected from redis")
})

process.on("SIGINT", () => {
    client.quit()
})
module.exports = client