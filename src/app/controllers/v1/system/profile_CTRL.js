"use strict";
const createError = require("http-errors");
const formidable = require("formidable");
const path = require("path");
const fs = require("fs");
// MODELS
const { Accounts, AccountSchema } = require("../../../models/v1/system/account_Model");
const { Profiles, ProfileSchema } = require("../../../models/v1/system/profile_Model");
const { Addresses, AddressesSchema } = require("../../../models/v1/profiles/addresses_Model");
// VALIDATORS
const { EditProfileSchema,EditEmailSchema, EditPasswordSchema } = require("../../../helpers/v1/Validations/Auth_Schema")
const { ProfileSaveSchema, ProfileUpdateSchema } = require("../../../helpers/v1/Validations/profiles_Validators")
const { AddressesSaveSchema, IdAddressesSchema } = require("../../../helpers/v1/Validations/addresses_Validators")
const { SearchScheme } = require("../../../helpers/v1/Validations/SearchByQuery_Validators")
// METHODS TO PERSONAL PROFILE
async function EditPassword(req, res, next) {
  try {
    const user = req.payload
    const result = await EditPasswordSchema.validateAsync(req.body)
    const account = await Accounts.findById(user.aud)
    if (!account) return next(createError.NotFound("Cuenta no registrada."))
    const isMatch = await account.comparePassword(result.password)
    if (!isMatch) throw createError.Unauthorized(`Password invalido.`);
    if (!account.vigence) throw createError.Unauthorized(`Cuenta temporalmente bloqueada. Contacta con el administrador para renovar tu acceso.`)
    account.set("password", result.newPassword)
    const change = await account.save();
    res.status(200).send({ message: `Haz cambiado tu password satisfactoriamente.` })
  } catch (error) {
    console.log(error);
    
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}
async function EditEmail(req, res, next) {
  try {
    const user = req.payload
    const result = await EditEmailSchema.validateAsync(req.body)
    const account = await Accounts.findById(user.aud)
    if (!account) return next(createError.NotFound("Cuenta no registrada."))
    const isMatch = await account.comparePassword(result.password)
    if (!isMatch) throw createError.Unauthorized(`Password invalido.`);
    if (!account.vigence) throw createError.Unauthorized(`Cuenta temporalmente bloqueada. Contacta con el administrador para renovar tu acceso.`)
    console.log(isMatch)
    await account.update({"email": result.email})
    // account.set("email", result.email)
    // const change = await account.save().then();
    res.status(200).send({ message: `Haz cambiado tu email satisfactoriamente` });
  } catch (error) {
    console.log(error);
    
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}

async function Identity(req, res, next) {
  try {
    const user = req.payload;
    const account = await Accounts.findById(user.aud).select("profile").exec();
    if (!account) return next(createError.NotFound("Usuario no registrado!."));
    const profile = await Profiles.findById(account.profile)
    // .select("estado_civil grado_instruccion state nombres apellidos dni_ce fecha_nacimiento createdAt updatedAt imagen").exec();
    if (!profile) return next(createError.NotFound("Cuenta de usuario no registrada."));
    const accounts = await Accounts.find({profile: profile._id}).select("email state kind").exec();
  //   profile.aggregate([
  //     {
  //         $lookup: {
  //             from: "personalincome",
  //             localfield: "_id",
  //             foreignField: "_id",
  //             as: "ingresos"
  //         }
  //     }
  // ])
    res.status(200).send(Object.assign(profile.toJSON(), {accounts: accounts} ));
  } catch (error) {
    console.log(error);
    
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}
async function EditProfile(req, res, next) {

  const form = formidable({ multiples: true })
  form.parse(req, async (err, fields, files) => {
    try {
      const user = req.payload;
      const ValidSchemeEditProfile = await EditProfileSchema.validateAsync(fields);
      const account = await Accounts.findById(user.aud).select("_id profile");
      const profile = await Profiles.findById(account.profile);
      if (!profile || !account) {
        return next(createError.NotFound("Usuario no registrado!."));
      }
      if(files.imagen) {
        if(err) return next(createError.InternalServerError(`Ha ocurrido un error miestras cargaba la imagen.`))
        if(!(/image\/+(jpg|png|heic|jpeg)$/i).test(files.imagen.type)) {
          return next(createError.NotAcceptable(`Tipo de imagenes aceptables jpg, png, heic y jpeg.`))
        }
        let oldPath = files.imagen.path;
        let newPath = path.join(__dirname, "../../../../../storage/avatars") + "/" + account.profile+"."+files.imagen.type.match(/(jpg|png|heic|jpeg)/g)[0];
        let rawData = fs.readFileSync(oldPath);
        fs.writeFile(newPath, rawData, async function (err) {
            if (err) return next(createError.InternalServerError(`Ha ocurrido un error miestras se guardaba la imagen.`))
        });
        ValidSchemeEditProfile.imagen = "/storage/avatars/" + account.profile+"."+files.imagen.type.match(/(jpg|png|heic|jpeg)/g)[0]
      }

      await Profiles.updateOne({_id: account.profile}, {$set: ValidSchemeEditProfile});
      const prof = await Profiles.findById(account.profile).select("nombres apellidos telefono grado_instruccion dni_ce fecha_nacimiento createdAt updatedAt imagen");

      res.status(200).send(prof);
    } catch (error) {
      if (error.isJoi) error.status = 422;
      if (error.code === 11000) return next(createError.Conflict("El usuario ya esta registrado. "))
      next(error);
    }
   })
}
async function EditAddresses(req, res, next) {
  try {
    const user = req.payload;
    const Id = await IdAddressesSchema.validateAsync(req.query);
    const result = await AddressesSaveSchema.validateAsync(req.body);
    const addresses = await Addresses.findByIdAndUpdate(Id, {
      contry: result.contry,
      departament: result.departament,
      province: result.province,
      district: result.district,
      addresses: result.addresses,
      phones: result.phones,
      latitude: result.latitude,
      altitude: result.altitude,
      longitude: result.longitude,
      vigence: result.vigence,
      profile: result.profile,
    });
    if (!addresses)
      return next(createError.NotFound("Address not registered."));
    const changeAddresses = await addresses.save();
    res.status(200).send(changeAddresses.toJSON());
  } catch (error) {
    next(error);
  }
}
async function GetAddresses(req, res, next) {
  try {
    const user = req.payload;
    const addresses = await Addresses.findById(user.aud);
    if (!addresses)
      return next(createError.NotFound("Address not registered."));
    res.status(200).send(addresses.toJSON());
  } catch (error) {
    next(error);
  }
}
// METHODS TO USER PROFILE
/**
 * @swagger
 * /get:
 *    description: Muestra la lista de todos usuarios vigentes
 *    responses:
 *      '200':
 *          description: La respuesta es satisfactoria
 */
async function ReadUser(req, res, next) {
  try {
    const queries = await SearchScheme.validateAsync(req.query);
    // const profiles = await Profiles.aggregate([
    //   {
    //     $lookup: {
    //       from: "accounts",
    //       localField: "_id",
    //       foreignField: "profile",
    //       as: "cuentas"
    //     }
    //   }
    // ]).exec()
    const profiles = await Profiles
    .find((queries.search) ? { $text: { $search:queries.search } } : {})
    .skip(queries.page * queries.limit)
    .limit(queries.limit)
    .sort(queries.orderby)
    .select("state _id nombres apellidos imagen dni_ce fecha_nacimiento estado_civil grado_instruccion telefono createdAt updatedAt")
    if (!profiles) throw createError.NotFound("There is no registered user");

    var total = await Profiles.countDocuments();
    var total_pages = await Math.ceil((total/queries.limit));
    return res.status(200).send(
        {
            "page": queries.page,
            "per_page": queries.limit,
            "total": total,
            "total_pages": total_pages,
            "data": profiles
        }
    );
  } catch (error) {
    next(error);
  }
}
async function SaveUser(req, res, next) {
  const form = formidable({ multiples: true })
  form.parse(req, async (err, fields, files) => {
    try {
      const UserCreate = await ProfileSaveSchema.validateAsync(fields); 
      const profiles = await Profiles.findOne({dni_ce: UserCreate.dni_ce});
      const profile = new Profiles();
      if (profiles) throw createError.Conflict("Este usuario ya ha sido registrado.");
      if(files.imagen) {
        if(err) return next(createError.InternalServerError(`Ha ocurrido un error miestras cargaba la imagen.`))
        if(!(/image\/+(jpg|png|heic|jpeg)$/i).test(files.imagen.type)) {
          return next(createError.NotAcceptable(`Tipo de imagenes aceptables jpg, png, heic y jpeg.`))
        }
        let oldPath = files.imagen.path;
        let newPath = path.join(__dirname, "../../../../../storage/avatars") + "/" + profile._id+"."+files.imagen.type.match(/(jpg|png|heic|jpeg)/g)[0];
        let rawData = fs.readFileSync(oldPath);
        fs.writeFile(newPath, rawData, async function (err) {
            if (err) return next(createError.InternalServerError(`Ha ocurrido un error miestras se guardaba la imagen.`))
        });
      }
      profile.set("nombres", UserCreate.nombres);
      profile.set("apellidos", UserCreate.apellidos);
      profile.set("dni_ce", UserCreate.dni_ce);
      profile.set("fecha_nacimiento", UserCreate.fecha_nacimiento);
      profile.set("telefono", UserCreate.telefono);
      profile.set("imagen", (files.imagen)?"/storage/avatars/"+profile._id+"."+files.imagen.type.match(/(jpg|png|heic|jpeg)/g)[0]:"");
      profile.set("estado_civil", UserCreate.estado_civil);
      profile.set("grado_instruccion", UserCreate.grado_instruccion);
      profile.set("vigence", UserCreate.vigence);
      profile.set("state", UserCreate.state);
      const saveProfile = await profile.save();
      res.send(saveProfile);
    } catch (error) {
      if (error.isJoi) error.status = 422;
      if (error.code === 11000) return next(createError.Conflict("El usuario ya esta registrado. "))
      next(error);
    }
  })
}
async function UpdateUser(req, res, next) {
  const form = formidable({ multiples: true })
  form.parse(req, async (err, fields, files) => {
    try {
      if(!req.params.userId) {
        throw createError.NotFound("El ID del registro es requerido");
      }
      const UserCreate = await ProfileUpdateSchema.validateAsync(fields); 
      const profiles = await Profiles.findById(req.params.userId);
      if (!profiles) throw createError.NotFound("Usuario no registrado.");
      if(files.imagen) {
        if(err) return next(createError.InternalServerError(`Ha ocurrido un error miestras cargaba la imagen.`))
        if(!(/image\/+(jpg|png|heic|jpeg)$/i).test(files.imagen.type)) {
          return next(createError.NotAcceptable(`Tipo de imagenes aceptables jpg, png, heic y jpeg.`))
        }
        let oldPath = files.imagen.path;
        let newPath = path.join(__dirname, "../../../../../storage/avatars") + "/" + profile._id+"."+files.imagen.type.match(/(jpg|png|heic|jpeg)/g)[0];
        let rawData = fs.readFileSync(oldPath);
        fs.writeFile(newPath, rawData, async function (err) {
            if (err) return next(createError.InternalServerError(`Ha ocurrido un error miestras se guardaba la imagen.`))
        });
      }
      const saveProfile = await profiles.update({
        "nombres": UserCreate.nombres,
        "apellidos": UserCreate.apellidos,
        "dni_ce": UserCreate.dni_ce,
        "fecha_nacimiento": UserCreate.fecha_nacimiento,
        "telefono": UserCreate.telefono,
        "imagen": (files.imagen)?"/storage/avatars/"+profiles._id+"."+files.imagen.type.match(/(jpg|png|heic|jpeg)/g)[0]:"",
        "estado_civil": UserCreate.estado_civil,
        "grado_instruccion": UserCreate.grado_instruccion,
        "vigence": UserCreate.vigence,
        "state": UserCreate.state,
      });
      res.send(saveProfile);
    } catch (error) {
      if (error.isJoi) error.status = 422;
      if (error.code === 11000) return next(createError.Conflict("El usuario ya esta registrado. "))
      next(error);
    }
  })
}
async function DeleteUser(req, res, next) {
  try {
    if(!req.params.userId) {
      throw createError.NotFound("El ID del registro es requerido");
    }
    const profiles = await Profiles.findOneAndUpdate({_id: req.params.userId}, {vigence: false});
    if (!profiles) throw createError.NotFound("There is no registered user");
    res.send(profiles);
  } catch (error) {
    next(error);
  }
}
async function DeleteAccount(req, res, next) {
  try {
    const profiles = await Profiles.find().populate({
      path: "account",
      select: "email displayName role vigence createdAt updatedAt",
    });
    if (!profiles) throw createError.NotFound("There is no registered user");
    res.send(profiles);
  } catch (error) {
    next(error);
  }
}
async function UpdateAccount(req, res, next) {
  try {
    const profiles = await Profiles.find().populate({
      path: "account",
      select: "email displayName role vigence createdAt updatedAt",
    });
    if (!profiles) throw createError.NotFound("There is no registered user");
    res.send(profiles);
  } catch (error) {
    next(error);
  }
}
module.exports = {
  EditEmail,
  EditPassword,
  Identity,
  EditProfile,
  EditAddresses,
  GetAddresses,
  ReadUser,
  SaveUser,
  UpdateUser,
  DeleteUser,
  DeleteAccount,
  UpdateAccount,
};
