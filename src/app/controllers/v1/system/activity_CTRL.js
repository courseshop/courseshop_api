"use strict";
const createError = require("http-errors");
const { Activities, ActivitySchema } = require('../../../models/v1/system/activity_Model');
const { IdActivitySchema } = require("../../../helpers/v1/Validations/activity_Validators")
async function get (req, res, next) {
    try {
        const idActivitySchema = await IdActivitySchema.validateAsync(req.query)
        let finddb = await Activities.find((idActivitySchema.start && idActivitySchema.end)?{"createdAt": {"$gte": idActivitySchema.start, "$lt": idActivitySchema.end }}: '')
        return res.status(200).send(finddb);
    } catch (error) {
        return next(createError.InternalServerError(error.message))
    }
}

module.exports = {
    get
}