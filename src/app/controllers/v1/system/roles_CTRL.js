"use strict";
const createError = require("http-errors");
const { Role, RolesSchema, Privilege, PrivilegesSchema } = require('../../../models/v1/system/roles_Model');
const { IdSchema, RolesSaveSchema } = require('../../../helpers/v1/Validations/Roles_Validator');
async function get (req, res, next) {
    try {
        let finddb = await Role.find()
        return res.status(200).send(finddb);
    } catch (error) {
        return next(createError.InternalServerError(error))
    }

}
async function save (req, res, next) {
    try {
        const rolesSaveSchema =  await RolesSaveSchema.validateAsync(req.body)
        const exist = await Role.findOne({name: rolesSaveSchema.name})
        if(exist) return next(createError.Conflict(`There is a record with this name`))
        const units = new Role();
        units.set("name", rolesSaveSchema.name)
        units.set("privilege", rolesSaveSchema.privilege)
        units.set("state", rolesSaveSchema.state)
        units.set("vigence", rolesSaveSchema.vigence)
        const result = await units.save().then()
        return res.status(200).send({message: `It's done, unit registered`})
    } catch (error) {
        return next(createError.InternalServerError(error))
    }
}

async function update (req, res, next) {
    try {
        const idSchema = await IdSchema.validateAsync(req.query);
        const rolesSaveSchema =  await RolesSaveSchema.validateAsync(req.body)
        const exist = await Role.findOne({_id: idSchema.id})
        if(exist) {
            await exist.update({
                name: ( rolesSaveSchema.name)?  rolesSaveSchema.name:  exist.name,
                privilege: ( rolesSaveSchema.privilege)?  rolesSaveSchema.privilege:  exist.privilege,
                state: ( rolesSaveSchema.state)?  rolesSaveSchema.state:  exist.state,
                vigence: ( rolesSaveSchema.vigence)?  rolesSaveSchema.vigence:  exist.vigence,
            })
            return res.status(200).send({message: `It's done, unit updated`})
        }
        return next(createError.NotFound(`There isn't a record with this name`))
    } catch (error) {
        return next(createError.InternalServerError(error))
    }
}
async function remove (req, res, next) {
  
    try {
        const idSchema = await IdSchema.validateAsync(req.query);
        const exist = await Role.findOne({_id: idSchema.id})
        if(exist) {
            await exist.remove()
            return res.status(200).send({message: `It's done, unit removed`})
        }
        return next(createError.NotFound(`There isn't a record with this name`))
    } catch (error) {
        return next(createError.InternalServerError(error))
    }
}

module.exports = {
    get,
    save,
    update,
    remove
}