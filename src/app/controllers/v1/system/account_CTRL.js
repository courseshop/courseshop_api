"use strict";
const createError = require("http-errors");
const request = require('request');

const {
  AccountSchema,
  Accounts,
} = require("../../../models/v1/system/account_Model");
const {
  ProfileSchema,
  Profiles,
} = require("../../../models/v1/system/profile_Model");
const {
  SignUpSchema,
  SignInSchema,
  ResetPasswordSchema,
  ResetCodeSchema,
  EditPasswordSchema,
  EditEmailSchema,
  EditProfileSchema,
} = require("../../../helpers/v1/Validations/Auth_Schema");
const {
  GenerateAccessToken,
  VerifyAccessToken,
  GenerateRefreshToken,
  VerifyRefreshToken,
} = require("../../../helpers/JWT_helper");
const client = require("../../../config/init_redis");
const sendMail = require("../../../helpers/SendEmail_helper");

const crypto = require("crypto")

const eventEmitter = require("events").EventEmitter;
const nodeSchedule = require("node-schedule")
const emitter = new eventEmitter();
const moment = require("moment");
const { throws } = require("assert");
async function signin(req, res, next) {
  try {
    const result = await SignInSchema.validateAsync(req.body);
    const account = await Accounts.findOne({ email: result.email })
      .select("email password vigence state kind")
      .populate({ path: "profile", select: "imagen state vigence  role nombres apellidos" })
      .exec();
    if (!account) return next(createError.NotFound(`Usuario no resgistrado!.`));
    const isMatch = await account.comparePassword(result.password);
    if (!isMatch) throw createError.Unauthorized(`Email ó Password invalido.`);
    if (!account.state || !account.vigence)
      throw createError.Unauthorized(`Cuenta temporalmente bloqueada. Contacta con el administrador para renovar tu acceso!.`);
    if (!account.profile.vigence)
      throw createError.Unauthorized(`Usuario temporalmente bloqueada. Contacta con el administrador!.`);
    const token = await GenerateAccessToken(account.toJSON());
    const refreshToken = await GenerateRefreshToken(account.toJSON());
    account.vigence = undefined;
    account.state = undefined;
    account.profile.vigence = undefined;
    account.profile.state = undefined;
    res.send({ account: account, access_token: token, refresh_token: refreshToken });
  } catch (error) {
    if (error.isJoi) error.status = 422;
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}
async function signup(req, res, next) {
  try {
    const resultProfile = await SignUpSchema.validateAsync(req.body);
    const profile = new Profiles();
    const account = new Accounts();
    profile.set("nombres", resultProfile.nombres);
    profile.set("apellidos", resultProfile.apellidos);
    profile.set("dni_ce", resultProfile.dni_ce);
    profile.set("fecha_nacimiento", resultProfile.fecha_nacimiento);
    account.set("password", resultProfile.password);
    account.set("email", resultProfile.email);
    account.set("profile", profile._id);
    const saveAccount = await account.save();
    const saveProfile = await profile.save();
    const token = await GenerateAccessToken(account.toJSON());
    const refreshToken = await GenerateRefreshToken(account.toJSON());
    res.send({ account: saveAccount.toJSON(), access_token: token, refresh_token: refreshToken });
  } catch (error) {
    if (error.isJoi) error.status = 422;
    if (error.code === 11000) return next(createError.Conflict("Este email ya esta siendo usuado!."))
    
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}
async function signout(req, res, next) {
  try {
    const { refresh_token } = req.body;
    const userId = await VerifyRefreshToken(refresh_token);
    client.DEL(userId, (err, val) => {
      if (err) throw createError.InternalServerError();
      res.sendStatus(204);
    });
  } catch (error) {
   
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}
async function refreshToken(req, res, next) {
  try {
    const { refresh_token } = req.body;
    console.log(refresh_token);
    const userId = await VerifyRefreshToken(refresh_token);
    console.log(userId);
    const account = await Accounts.findById(userId).select("email state vigence").exec();
    const newToken = await GenerateAccessToken(account.toJSON());
    const newRefreshToken = await GenerateRefreshToken(account.toJSON());
    res.send({ access_token: newToken, refresh_token: newRefreshToken });
  } catch (error) {
   
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}
async function resetCode(req, res, next) {
  try {
    const result = await ResetCodeSchema.validateAsync(req.body);
    const account = await Accounts.findOne({ email: result.email }).select(
      "_id"
    );
    if (!account) return next(createError.NotFound("Usuario no registrado!."));
    // const code = (await Math.floor(Math.random() * (999999 - 99999 + 1))) + 99999;
    const code = (await crypto.randomBytes(3).toString("hex"));
    // console.table("Email", result.email, "Reset Code", code);
    await client.SET(String(account._id) + ".resetCode", code, "EX", 60 * 60);
    await sendMail(
      result.email,
      "Código de reseteo de Contraseña",
      "Codigo de Reseteo ",
      "Tu código de reseteo es: <strong>" +
      code +
      "</strong> \n <br><i>Tienes 1 Hora para cambiar tu contraseña.<i><br><br><br><strong>NOTE:</strong> If you don't know this message, delete this message."
    );
    res
      .status(200)
      .send({
        message: `Enviamos un email a ${result.email} con tu codigo de reseteo, Verifica tu bandeja de entrada`,
      });
  } catch (error) {
   
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}
async function resetPassword(req, res, next) {
  try {
    const result = await ResetPasswordSchema.validateAsync(req.body);
    const account = await Accounts.findOne({ email: result.email }).select(
      "_id"
    );
    if (!account) return next(createError.NotFound("Usuario no registrado!."));
    if (account.vigence)
      throw createError.Unauthorized(
        `Cuenta temporalmente bloqueada. Contacta con el administrador!.`
      );
    const code = await client.GET(
      String(account._id) + ".resetCode",
      (err, code) => {
        if (err) return next(createError.InsufficientStorage());
        console.log(code);
        console.log(result.code);
        if (code !== result.code) {
          return next(createError.Unauthorized("Código de reseteo inválido. Solicite un nuevo código de reseteo."));
        }
      }
    );
    if(code) {
      await client.DEL(String(account._id) + ".resetCode");
    }
    account.set("password", result.password);
    const change = await account.save();
    res.status(200).send({ message: `Reseteo de contraseña satisfactorio!.` });
  } catch (error) {
   
    if(error.status === 500) {
      next(createError.InternalServerError(`Algo ha salido mal, Intenta más tarde!.`));
    } else {
      next(error);
    }
  }
}

 
 

module.exports = {
  signin,
  signup,
  signout,
  refreshToken,
  resetCode,
  resetPassword
};
