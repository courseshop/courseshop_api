"use strict";
const createError = require("http-errors");
const { Addresses, AddressesSchema } = require('../../../models/v1/profiles/addresses_Model');
const { IdAddressesSchema, AddressesSaveSchema, AddressesUpdateSchema } = require("../../../helpers/v1/Validations/addresses_Validators")
async function get (req, res, next) {
    try {
        let finddb = await Addresses.where("vigence", true).find()
        return res.status(200).send(finddb);
    } catch (error) {
        return next(createError.InternalServerError(error))
    }

}
async function save (req, res, next) {
    try {
        const addressesSaveSchema =  await AddressesSaveSchema.validateAsync(req.body)
        const addresses = new Addresses();
        addresses.set("contry", addressesSaveSchema.contry)
        addresses.set("departament", addressesSaveSchema.departament)
        addresses.set("province", addressesSaveSchema.province)
        addresses.set("addresses", addressesSaveSchema.addresses)
        addresses.set("phones", addressesSaveSchema.phones)
        addresses.set("profile", addressesSaveSchema.profile)
        const result = await addresses.save().then()
        return res.status(200).send({message: `It's done, address registered` })
    } catch (error) {
        return next(createError.InternalServerError(error))
    }
}

async function update (req, res, next) {
    try {
        const idAddressesSchema =  await IdAddressesSchema.validateAsync(req.query)
        const addressesUpdateSchema =  await AddressesUpdateSchema.validateAsync(req.body)
        const exist = await Addresses.findOne({_id: idAddressesSchema.id})
        if(exist){
            await exist.update({
                contry:( addressesUpdateSchema.contry)? addressesUpdateSchema.contry:exist.contry,
                departament:( addressesUpdateSchema.departament)? addressesUpdateSchema.departament:exist.departament,
                province:( addressesUpdateSchema.province)? addressesUpdateSchema.province:exist.province,
                addresses:( addressesUpdateSchema.addresses)? addressesUpdateSchema.addresses:exist.addresses,
                phones:( addressesUpdateSchema.phones)? addressesUpdateSchema.phones:exist.phones,
                profile:( addressesUpdateSchema.profile)? addressesUpdateSchema.profile:exist.profile,
            })
            return res.status(200).send({message: `It's done, address updated` })
        }
        return next(createError.Conflict(`This record does not exist`))
    } catch (error) {
        return next(createError.InternalServerError(error))
    }
}
async function remove (req, res, next) {
    try {
        const idAddressesSchema =  await IdAddressesSchema.validateAsync(req.query)
        const exist = await Addresses.findOne({_id: idAddressesSchema.id})
        if(exist){
            if(idAddressesSchema.permanent) {
                await exist.remove()
            } else {
                await exist.update({
                    vigence:false
                })
            }
            return res.status(200).send({message: `It's done, address removed` })
        }
        return next(createError.Conflict(`This record does not exist`))
    } catch (error) {
        return next(createError.InternalServerError(error))
    }
}

module.exports = {
    get,
    save,
    update,
    remove
}