"use strict"
const mongoose = require('mongoose') ;
const GeolocalizationSchema = new mongoose.Schema({
    contry: { type: String, trim: true },
    departament: { type: String, trim: true },
    province: { type: String, trim: true },
    district: { type: String, trim: true },
    addresses: [{ type: String, trim: true }],
    phones: [{ type: String, trim: true}],
    latitude: { type: Number },
    altitude: { type: Number },
    longitude: { type: Number },
    vigence: { type: Boolean, default: true }, 
    state: { type: Boolean, default: true }, 
    profile: { type: mongoose.Schema.Types.ObjectId, ref: 'profiles'}
}, { 
    timestamps: true, 
    versionKey: false 
});
const Geolocalization = mongoose.model("geolocalization", GeolocalizationSchema, "geolocalization");
Geolocalization.watch().on("change", (data) => {
    // console.log("change data:", data);
});
module.exports = {
    GeolocalizationSchema,
    Geolocalization
}