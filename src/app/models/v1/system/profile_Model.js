"use strict"
const mongoose = require('mongoose') ;
const { AccountModel } = require('./account_Model');
const ProfileSchema = new mongoose.Schema({
    nombres: { type: String, lowercase: true, trim: true  },
    apellidos: { type: String, lowercase: true, trim: true  },
    dni_ce: { type: String, lowercase: true, trim: true},
    fecha_nacimiento: { type:  Date},
    addresses: [{ type: mongoose.Schema.Types.ObjectId, ref: 'geolocalization' }],
    imagen: { type: String },
    estado_civil: { type: String, default: 'soltero', enum: ["soltero", "casado", "divorciado", "viudo"] },
    grado_instruccion: { type: String, default: 'sin nivel', enum: ["sin nivel", "pre-escolar", "primaria", "secundaria", "superior"] },
    vigence: { type: Boolean,  default: true },
    state: { type: Boolean,  default: true },
}, { timestamps: true, versionKey: false });

ProfileSchema.index({"$**": "text"});
// ProfileSchema.post("aggregate", function(docs, next) {
// });
ProfileSchema.pre('findOne',  async function() {
  });
const Profiles = mongoose.model("profiles", ProfileSchema, "profiles");
Profiles.watch().on("change", (data) => {
});


module.exports = {
    ProfileSchema,
    Profiles
}