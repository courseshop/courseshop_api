"use strict"
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const { ProfileModel, Profiles } = require('./profile_Model');
const sendMail = require("../../../helpers/SendEmail_helper");
const AccountSchema = new mongoose.Schema({
    email: { type: String, unique: true, required: true}, 
    password: { type: String, min: 8, required: true}, 
    vigence: { type: Boolean, default: true}, 
    state: { type: Boolean, default: true}, 
    kind: { type: String, default:'credentials'}, 
    role: { type: mongoose.Schema.Types.ObjectId, ref: 'roles' }, 
    profile: { type: mongoose.Schema.Types.ObjectId, ref: 'profiles' }, 
}, { timestamps: true, versionKey: false });

AccountSchema.methods.comparePassword = async function(candidatePassword) {
    try {
        const isMatch = bcrypt.compare(candidatePassword, this.password);
        this.password = undefined 
        return await isMatch
    } catch (error) {
         throw error;
    }
}

AccountSchema.pre('save', function save(next) {
    const user  = this;
    this.populate({path: "profile" }).execPopulate();
    if(!user.isModified('password')) {
        return next();
    }
    bcrypt.genSalt(10, (err, salt) => {
        if(err) {
            next(err);
        }
        bcrypt.hash(user.password, salt, (err, hash) => {
            if(err) {
                next(err);
            }
            user.password = hash;
            next();
        });
    });
})

AccountSchema.post('save', async function (doc) { 
    console.log("post.save: ", doc);
    
});
const Accounts = mongoose.model("accounts", AccountSchema, "accounts");
Accounts.watch().on("change", (data) => {
    // console.log("change data:", data);
});

module.exports = {
    AccountSchema,
    Accounts
}