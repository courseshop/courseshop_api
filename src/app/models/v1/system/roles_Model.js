"use strict"
const mongoose = require('mongoose') ;
const RolesSchema = new mongoose.Schema({
    name: { type: String, trim: true, required: true, unique:true },
    state: { type: Boolean, default: true },
    vigence: { type: Boolean, default: true },
    privilege: {type: mongoose.Schema.Types.ObjectId, ref: 'privileges'}
}, { timestamps: true, versionKey: false });
RolesSchema.pre("init", (doc) => {
    // console.log("pre init : ", doc);
})

const Role = mongoose.model("roles", RolesSchema, "roles");
Role.watch().on("change", (data) => {
    // console.log("change data:", data);
    // Activities.create(data)
});


const PrivilegesSchema = new mongoose.Schema({
    schema: { type: String, trim: true, required: true },
    actions: { type: Array }
}, { timestamps: true });

const Privilege = mongoose.model("privileges", PrivilegesSchema, "privileges");
Privilege.watch().on("change", (data) => {
    // console.log("change data:", data);
});


module.exports = {
    RolesSchema,
    Role,
    PrivilegesSchema,
    Privilege
}