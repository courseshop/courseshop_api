"use strict"
const mongoose = require('mongoose') ;
const AddressesSchema = new mongoose.Schema({
    contry: { type: String, trim: true },
    departament: { type: String, trim: true },
    province: { type: String, trim: true },
    addresses: [{ type: String, trim: true }],
    phones: [{ type: String, trim: true}],
    vigence: { type: Boolean, default: true }, 
    profile: { type: mongoose.Schema.Types.ObjectId, ref: 'profiles', required: true}
}, { timestamps: true, versionKey: false });
const Addresses = mongoose.model("addresses", AddressesSchema, "addresses");
Addresses.watch().on("change", (data) => {
    console.log("change data:", data);
});
module.exports = {
    AddressesSchema,
    Addresses
}